package com.example.tasksecond;

public class Items {
    private String Title;
    private String Price;
    private  int Image;

    public Items() {
    }

    public Items(String title, String price, int image) {
        Title = title;
        Price = price;
        Image = image;
    }



    public String getTitle() {
        return Title;
    }

    public String getPrice() {
        return Price;
    }

    public int getImage() {
        return Image;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public void setImage(int image) {
        Image = image;
    }
}
