package com.example.tasksecond;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<Items> itemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        itemsList = new ArrayList<>();
        itemsList.add(new Items("Apple","Rs.50",R.drawable.apple));
        itemsList.add(new Items("Colgate","Rs.10",R.drawable.colgate));
        itemsList.add(new Items("Corn","Rs.30",R.drawable.corn));
        itemsList.add(new Items("ToothBrush","Rs.20",R.drawable.toothbrush));
        itemsList.add(new Items("Grapes","Rs.50",R.drawable.grapes));
        itemsList.add(new Items("Watermelon","Rs.40",R.drawable.watermelon));
        itemsList.add(new Items("Apple","Rs.30",R.drawable.apple));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerID);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this, itemsList);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(myAdapter);

    }
}